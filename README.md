# README #

Tu można dodawać rózne pliki wspomagające serwisy marketingowo. Dzięki temu zachowamy ich historię i integralność.

# UWAGA! #
# UWAGA! #
# UWAGA! #
To repozytorium jest _PUBLICZNE_! Cały świat może go dokładnie oglądać. Nie umieszczaj tu niczego, co nie powinno być widoczne. 
W razie niepewności skontaktuj się z JFK.

# Edycja #
Aby edytować jakiś plik, naciśnij jego nazwę (najczęściej ads.txt) na liście powyżej lub tej znajdującej się [TU](https://bitbucket.org/monstermedia/marketing/src).
Po prawej stronie na górze jest przycisk "Edit". W polu tekstowym można wprowadziź zmiany, po czym należy kliknąć Commit. 
W treść commita  należy wpisać opis  zmiany(np. dodanie/usunięcie reklamodawcy XYZ) i ponownie wcisnąć commit.

# Opisy commitów #
Ważne jest, by opisy commitów były jak najbardziej treściwe. Dzięki temu, za jakiś czas, będzie można prześliedzić ewolucję pliku tylko za pomocą samej listy opisów, bez konieczności mozolnego sprawdzania każdego pliku.
O wiele czytelniejsza jest lista typu:


 - Dodanie reklamodawcy XYZ
 - Usunięcie Firma SA - zakonczenie wspolpracy dd.mm.yyyy
 - Dodanie Google (jakis wyróżnik usługi)
 
 niż listę:

 - ads.txt edited with bitbucket
 - ads.txt edited with bitbucket
 - ads.txt edited with bitbucket
